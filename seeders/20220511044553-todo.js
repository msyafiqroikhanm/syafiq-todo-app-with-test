"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Todos", [
      {
        userId: 1,
        name: "Latihan test",
        schedule: "2022-06-06",
        completed: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 2,
        name: "Latihan test v2",
        schedule: "2022-06-06",
        completed: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete(
      "Todos",
      {},
      { truncate: true, restartIdentity: true }
    );
  },
};
