const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const TodoController = require("../controllers/todo.controller");
const { User } = require("../models");
const passport = require("../passport");

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  // (req, res, next) => {
  //   try {
  //     if (!req.headers.authorization) {
  //       throw {
  //         status: 401,
  //         message: "Unauthorized request",
  //       };
  //     } else {
  //       const user = jwt.verify(req.headers.authorization, "qweqwe");
  //       if (user) {
  //         const loggedInUser = User.findOne({
  //           where: { email: user.email },
  //         });
  //         if (!loggedInUser) {
  //           throw {
  //             status: 401,
  //             message: "Unauthorized request",
  //           };
  //         }
  //         req.user = user;
  //         next();
  //       } else {
  //         throw {
  //           status: 401,
  //           message: "Unauthorized request",
  //         };
  //       }
  //     }
  //   } catch (err) {
  //     next(err);
  //   }
  // },
  (req, res, next) => {
    const errors = [];
    if (!req.body.name) {
      errors.push("Name is required");
    }
    if (!req.body.schedule) {
      errors.push("Schedule is required");
    }
    if (req.body.completed !== true && req.body.completed !== false) {
      errors.push("Completed is required");
    }
    if (new Date(req.body.schedule) < new Date()) {
      errors.push("Schedule should be greater than today");
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors,
      });
    } else {
      next();
    }
  },
  TodoController.create
);

router.get(
  "/",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (user) {
          req.user = user;
          next();
        } else {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
      }
    } catch (err) {
      next(err);
    }
  },
  TodoController.list
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      } else {
        const user = jwt.decode(req.headers.authorization);
        if (!user) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = user;
        next();
      }
    } catch (err) {
      next(err);
    }
  },
  TodoController.getById
);

router.put(
  "/:id",
  (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      const user = jwt.decode(req.headers.authorization);
      if (!user) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      req.user = user;
      next();
    } catch (err) {
      next(err);
    }
  },
  (req, res, next) => {
    const errors = [];
    if (!req.body.name) {
      errors.push("Name is required");
    }
    if (!req.body.schedule) {
      errors.push("Schedule is required");
    }
    if (req.body.completed !== true && req.body.completed !== false) {
      errors.push("Completed is required");
    }
    if (new Date(req.body.schedule) < new Date()) {
      errors.push("Schedule should be greater than today");
    }

    if (errors.length > 0) {
      next({
        status: 400,
        message: errors,
      });
    } else {
      next();
    }
  },
  TodoController.update
);

module.exports = router;
