const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const app = require("../app");
// sebelum test, kita butuh data user
let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync("Qweqwe123", salt);
  await queryInterface.bulkInsert("Users", [
    {
      email: "lifan@mail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      email: "syafiq@mail.com",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);
  await queryInterface.bulkInsert("Todos", [
    {
      userId: 1,
      name: "Latihan test",
      schedule: "2022-06-06",
      completed: false,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      userId: 2,
      name: "Latihan test v2",
      schedule: "2022-06-06",
      completed: false,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]);
  token = jwt.sign(
    {
      id: 1,
      email: "lifan@mail.com",
    },
    "qweqwe"
  );
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true }
  );
  await queryInterface.bulkDelete(
    "Todos",
    {},
    { truncate: true, restartIdentity: true }
  );
});

describe("POST Todo", () => {
  it("success", (done) => {
    request(app)
      .post("/todos")
      .set("authorization", token)
      .send({
        name: "Melakukan testing API",
        schedule: "2022-06-06",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Succesfully create todo");
          done();
        }
      });
  });
  it("No auth", (done) => {
    request(app)
      .post("/todos")
      .send({
        name: "Melakukan testing API",
        schedule: "2022-06-06",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
  it("Invalid auth token", (done) => {
    request(app)
      .post("/todos")
      .set("authorization", "qweqwe")
      .send({
        name: "Melakukan testing API",
        schedule: "2022-06-06",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
  it("Required field violation", (done) => {
    request(app)
      .post("/todos")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message.length).toBe(3);
          expect(res.body.message.includes("Name is required")).toBe(true);
          expect(res.body.message.includes("Schedule is required")).toBe(true);
          expect(res.body.message.includes("Completed is required")).toBe(true);
          done();
        }
      });
  });

  it("Schedule violation", (done) => {
    request(app)
      .post("/todos")
      .set("authorization", token)
      .send({
        name: "Melakukan testing API",
        schedule: "2022-01-01",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message.length).toBe(1);
          expect(
            res.body.message.includes("Schedule should be greater than today")
          ).toBe(true);
          done();
        }
      });
  });
});

describe("GET Todo", () => {
  it("success", (done) => {
    request(app)
      .get("/todos")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(Array.isArray(res.body)).toBe(true);
          done();
        }
      });
  });
  it("no auth", (done) => {
    request(app)
      .get("/todos")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
  it("invalid token", (done) => {
    request(app)
      .get("/todos")
      .set("authorization", "qweqwe")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });
});

describe("GET Todo By Id", () => {
  it("Success", (done) => {
    request(app)
      .get("/todos/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("name");
          expect(res.body).toHaveProperty("schedule");
          expect(res.body).toHaveProperty("completed");
          done();
        }
      });
  });

  it("No Authorization Token", (done) => {
    request(app)
      .get("/todos/1")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Authorization Token", (done) => {
    request(app)
      .get("/todos/1")
      .set("authorization", "token")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Accessed By Another User", (done) => {
    request(app)
      .get("/todos/2")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Todo Not Found", (done) => {
    request(app)
      .get("/todos/4")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Todo Not Found");
          done();
        }
      });
  });
});

describe("UPDATE Todo", () => {
  it("Success", (done) => {
    request(app)
      .put("/todos/1")
      .set("authorization", token)
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-05-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Successfully update todo");
          done();
        }
      });
  });

  it("No Authorization Token", (done) => {
    request(app)
      .put("/todos/1")
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-05-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Invalid Authorization Token", (done) => {
    request(app)
      .put("/todos/1")
      .set("authorization", "token")
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-05-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Updated by Another User", (done) => {
    request(app)
      .put("/todos/2")
      .set("authorization", "token")
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-05-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized request");
          done();
        }
      });
  });

  it("Todo Not Found", (done) => {
    request(app)
      .put("/todos/4")
      .set("authorization", token)
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-05-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Todo Not Found");
          done();
        }
      });
  });

  it("Required Field Violation", (done) => {
    request(app)
      .put("/todos/1")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(Array.isArray(res.body.message)).toBe(true);
          expect(res.body.message.length).toBe(3);
          expect(res.body.message.includes("Name is required")).toBe(true);
          expect(res.body.message.includes("Schedule is required")).toBe(true);
          expect(res.body.message.includes("Completed is required")).toBe(true);
          done();
        }
      });
  });

  it("Schedule Less Than Today", (done) => {
    request(app)
      .put("/todos/1")
      .set("authorization", token)
      .send({
        name: "Melakukan testing API v2",
        schedule: "2022-04-25",
        completed: false,
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(400);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message.length).toBe(1);
          expect(
            res.body.message.includes("Schedule should be greater than today")
          ).toBe(true);
          done();
        }
      });
  });
});
