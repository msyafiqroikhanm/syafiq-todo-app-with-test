# Todo API Contract

## API Contract (26 April 2022)

### POST /login

#### Success

- Request

```json
{
  "email": "lifan@mail.com",
  "password": "Qweqwe123"
}
```

- Response 200

```json
{
  "token": "${string}"
}
```

#### Wrong Password

- Request

```json
{
  "email": "lifan@mail.com",
  "password": "Qweqwe"
}
```

- Response (401)

```json
{
  "message": "Invalid email or password"
}
```

#### Wrong email

- Request

```json
{
  "email": "lifan@mail.co",
  "password": "Qweqwe123"
}
```

- Response (401)

```json
{
  "message": "Invalid email or password"
}
```

### POST /todos

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "name": "Melakukan testing API",
  "schedule": "2022-06-06",
  "completed": false
}
```

- Response

```json
{
  "message": "Successfully create todo"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Body

```json
{
  "name": "Melakukan testing API",
  "schedule": "2022-06-06",
  "completed": false
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid auth token

- Headers

```json
{
  "authorization": "qweqwe"
}
```

- Body

```json
{
  "name": "Melakukan testing API",
  "schedule": "2022-06-06",
  "completed": false
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Required Field Violation

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{}
```

- Response

```json
{
  "message": [
    "Name is required",
    "Schedule is required",
    "Completed is required"
  ]
}
```

#### Schedule violation

- Headers

```json
{
  "authorization": "${string}"
}
```

- Body

```json
{
  "name": "Melakukan testing API",
  "schedule": "2022-01-01",
  "completed": false
}
```

- Response

```json
{
  "message": ["Schedule should be greater than today"]
}
```

### GET /todos

#### Success

- Headers

```json
{
  "authorization": "${string}"
}
```

- Response

```json
[
  {
    "name": "Melakukan testing API",
    "completed": false
  }
]
```

#### No auth token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid token

- Headers

```json
{
  "authorization": "qweqwe"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

## API Contract (10 Mei 2022)

### GET /todos/:id

#### Success

- Headers

```json
{
  "authorization": "${token}"
}
```

- Response (200)

```json
{
  "name": "Melakukan testing API",
  "schedule": "2022-05-22",
  "completed": false
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Invalid Authorization Token

- Headers

```json
{
  "authorization": "token"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Accessed By Another User

- Headers

```json
{
  "authorization": "${token}"
}
```

- Response (401)

```json
{
  "message": "Unauthorized request"
}
```

#### Todo Not Found

- Headers

```json
{
  "authorization": "${token}"
}
```

- Response (404)

```json
{
  "message": "Todo Not Found"
}
```

### UPDATE /todos/:id

#### Success

- Headers

```json
{
  "authorization": "${token}"
}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-05-25",
  "completed": false
}
```

- Response (200)

```json
{
  "messsage": "Successfully update todo"
}
```

#### No Authorization Token

- Headers

```json
{}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-05-25",
  "completed": false
}
```

- Response (401)

```json
{
  "messsage": "Unauthorized request"
}
```

#### Invalid Authorization Token

- Headers

```json
{
  "authorization": "token"
}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-05-25",
  "completed": false
}
```

- Response (401)

```json
{
  "messsage": "Unauthorized request"
}
```

#### Updated by Another User

- Headers

```json
{
  "authorization": "token"
}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-05-25",
  "completed": false
}
```

- Response (401)

```json
{
  "messsage": "Unauthorized request"
}
```

#### Todo Not Found

- Headers

```json
{
  "authorization": "token"
}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-05-25",
  "completed": false
}
```

- Response (404)

```json
{
  "messsage": "Todo Not Found"
}
```

#### Required Field Violation

- Headers

```json
{
  "authorization": "${token}"
}
```

- Request

```json
{}
```

- Response (400)

```json
{
  "message": [
    "Name is required",
    "Schedule is required",
    "Completed is required"
  ]
}
```

#### Schedule less than today

- Headers

```json
{
  "authorization": "${token}"
}
```

- Request

```json
{
  "name": "Melakukan testing API v2",
  "schedule": "2022-04-25",
  "completed": false
}
```

- Response (400)

```json
{
  "messsage": ["Schedule should be greater than today"]
}
```
