const { Todo } = require("../models");

class TodoController {
  static async create(req, res, next) {
    try {
      await Todo.create({
        name: req.body.name,
        schedule: req.body.schedule,
        completed: req.body.completed,
        userId: req.user.id,
      });
      res.status(201).json({
        message: "Succesfully create todo",
      });
    } catch (err) {
      next(err);
    }
  }

  static async list(req, res, next) {
    try {
      const todos = await Todo.findAll({
        where: {
          userId: req.user.id,
        },
        attributes: ["name", "completed"],
      });
      res.status(200).json(todos);
    } catch (err) {
      next(err);
    }
  }

  static async getById(req, res, next) {
    try {
      const todo = await Todo.findOne({
        where: { id: req.params.id },
        attributes: ["userId", "name", "schedule", "completed"],
      });

      if (!todo) {
        throw {
          status: 404,
          message: "Todo Not Found",
        };
      }
      if (todo.userId !== req.user.id) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      delete todo.dataValues.userId;
      res.status(200).json(todo);
    } catch (err) {
      next(err);
    }
  }

  static async update(req, res, next) {
    try {
      const todo = await Todo.findByPk(req.params.id);
      if (!todo) {
        throw {
          status: 404,
          message: "Todo Not Found",
        };
      }
      if (todo.userId !== req.user.id) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      todo.set({
        name: req.body.name,
        schedule: req.body.schedule,
        completed: req.body.completed,
      });
      res.status(200).json({ message: "Successfully update todo" });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = TodoController;
